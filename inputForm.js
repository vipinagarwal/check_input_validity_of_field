handleInput(event){       
    if(event.target.name==='FirstName'){
        this.contactRecord['FirstName']=event.target.value;
    }else if(event.target.name==='MiddleName'){
        this.contactRecord['MiddleName']=event.target.value;
    }
}
// This will check the input field is correct or not if not it will show an error.
isValid(){
        let valid = false;
        let isAllValid = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        valid = isAllValid;
        console.log('Valid Org Info ' + valid);
        return valid;
    }